package Ap.qus.abstraction;

public class Employee extends Person {

	private int employeeId;

	public Employee(String name, String gender, int employeeId) {
		super(name, gender);
		this.employeeId = employeeId;
	}

	@Override
	public void work() {
		
		if (employeeId == 0) {
			System.out.println("Not work");

		} else {
			System.out.println("Employee working!!");
		}

	}
}
