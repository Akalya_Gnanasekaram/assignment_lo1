package Ap.qus.interface1;

public class Luxury implements Hall {

	@Override
	public int getPrice() {
		return 100000;
	}

	@Override
	public String getColour() {
		return "White";
	}

	@Override
	public String getDecoration() {
		return "Grand Decoration";
	}

	@Override
	public double getFront_Area(double x, double y) {
		return x * y;
	}

	@Override
	public String getFunction_Type() {
		return "Wedding";
	}

}
