package Ap.qus.constructor;

public class Student {

	public String name;
	public int age;

	public Student() {// default constructor
	}

	public Student(String name) {// single parameter constructor
		this.name = name;
	}

	public Student(String name, int age) {// multi parameter constructor
		this.name = name;
		this.age = age;
	}

	public void printDetails() {
		// System.out.println("This is a Student class");
		age=20;
		System.out.println("Hi " + name + " you are " + age + " years old.");
	}

}
