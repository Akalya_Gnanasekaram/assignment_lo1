package Ap.qus.polymorpism;

//sup class shapes
class Shapes {
	public double getarea() {
		return 0;
	}
}

//sub class triangle
class Triangle extends Shapes {
	private double height;
	private double base;

	public Triangle(double h, double b) {
		this.height = h;
		this.base = b;
	}

	public double getarea() {
		return 0.5 * height * base;
	}
}

//sub class circle
class circle extends Shapes {
	private double radius;

	public circle(double r) {
		this.radius = r;
	}

	public double getarea() {
		return 3.14 * radius * radius;
	}
}

//main class
public class PolymorpismTest1 {

	public static void main(String[] args) {

		Shapes[] shapes = new Shapes[2];
		shapes[0] = new circle(14);
		shapes[1] = new Triangle(9, 36);

		System.out.println("Area of circle :" + shapes[0].getarea());
		System.out.println("Area of Triangle :" + shapes[1].getarea());
	}

}
